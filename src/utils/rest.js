/**
 * Created by majid on 12/28/16.
 */
import { notification, Select, Tooltip } from 'antd';
import React from 'react';
import coreFetch from './../core/fetch';
import { apiService } from '../var';

const Option = Select.Option;


const openNotificationWithIcon = (type, message, description) => {
  try {
    if (process.env.BROWSER) {
      if(Array.isArray(description.message)){
        description.message.map(item => {
          notification[type]({
            message: item.message.toString(),
          });
        })
      } else {
        notification[type]({
          message: description.toString(),
        });
      }
    } else {
      console.log(type, message);
    }
  } catch (e) {

  }
};

export function setCookie(name, value, hours) {
  if (process.env.BROWSER) {
    let expires = '';
    if (hours) {
      const date = new Date();
      date.setTime(date.getTime() + hours * 60 * 60 * 1000);
      expires = `; expires=${date.toGMTString()}`;
    }
    document.cookie = `${name}=${value}${expires}; path=/`;
  } else {
    // console.log('You are not supposed to set a cookie at server');
  }
}

export function readCookieFrom(cookies, name) {
  const nameEQ = `${name}=`;
  const ca = cookies.split(';');
  let i = 0;
  const length = ca.length;
  for (; i < length; i += 1) {
    let c = ca[i];
    while (c.charAt(0) === ' ') {
      c = c.substring(1, c.length);
    }
    if (c.indexOf(nameEQ) === 0) {
      return c.substring(nameEQ.length, c.length);
    }
  }
  return null;
}

export function getCookie(name, cookie) {
  if (process.env.BROWSER) {
    return readCookieFrom(document.cookie, name);
  }
  if (cookie) {
    return readCookieFrom(cookie, name);
  }
  return null;
}

export function deleteCookie(name) {
  setCookie(name, '', -1);
}

export function validateEmail(email) {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

export function isNumber(number) {
  const isnum = /^\d+$/;
  return isnum.test(number);
}

export const FetchData = async (
  token,
  url,
  fetch = coreFetch,
  redirectToLogin = true
) => {
  const defaults = {
    method: 'GET',
  };
  if(token) defaults.headers = {token};
  if (!fetch) fetch = coreFetch;
  const resp = await fetch(url, defaults, fetch);
  const data = await resp.json();
  if ((data.code === 401 || data.code === 403) && redirectToLogin) {
    if (process.env.BROWSER) {
      window.location = apiService.loginUrl;
    }
  } else if (data.code !== 200 && data.error) {
    throw data.error.toString();
  }
  if(data.response){
    return data.response;
  } else {
    return data;
  }
};

export const DeleteData = async (token, url, fetch = coreFetch, redirectToLogin = true) => {
  const defaults = {
    method: 'DELETE',
    headers: { token },
  };
  const resp = await fetch(url, defaults);
  const data = await resp.json();

  if ((data.code === 401 || data.code === 403) && redirectToLogin) {
    window.location = apiService.loginUrl;
  } else if (data.code === 200) {
    openNotificationWithIcon('success', 'Success Delete', data.response.message);
  } else {
    openNotificationWithIcon('error', 'Error Delete', data.error);
    if(data.error) throw data.error.toString();
  }
  return data;
};

export const PostData = async (token = false, url, body, fetch = false, showNotification = true, redirectToLogin = true) => {
  body = JSON.stringify(body);

  const defaults = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json;charset=utf-8',
      Accept: 'application/json;charset=utf-8',
    },
    body,
  };
  if (token) defaults.headers.token = token;
  if (!fetch) fetch = coreFetch;
  const resp = await fetch(url, defaults);
  const data = await resp.json();
  if ((data.code === 401 || data.code === 403) && redirectToLogin) {
    if (process.env.BROWSER) {
      window.location = apiService.loginUrl;
    }
  } else if (data.code === 200 && data.response && showNotification) {
    openNotificationWithIcon('success', 'Success', data.response.message);

  } else if (showNotification) {
    openNotificationWithIcon('error', 'Error', data.error);
    if(data.error) throw data.error.toString();
  }
  return data;
};

export const DownloadData = async (token, url, body, fetch = coreFetch, redirectToLogin = true) => {
  body = JSON.stringify(body);
  const defaults = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json;charset=utf-8',
      token,
    },
    body,
  };
  if (!fetch) fetch = coreFetch;

  const resp = await fetch(url, defaults);
  const data = resp.blob();
  return data;
};

export const DownloadDataGet = async (token, url, fetch = coreFetch, redirectToLogin = true) => {
  const defaults = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json;charset=utf-8',
      token,
    },
  };
  if (!fetch) fetch = coreFetch;

  const resp = await fetch(url, defaults);
  const data = resp.blob();
  return data;
};

export const PutData = async (token, url, body, fetch = coreFetch, redirectToLogin = true) => {
  body = JSON.stringify(body);
  const defaults = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json;charset=utf-8',
      Accept: 'application/json;charset=utf-8',
      token,
    },
    body,
  };

  const resp = await fetch(url, defaults);
  const data = await resp.json();
  if ((data.code === 401 || data.code === 403) && redirectToLogin) {
    if (process.env.BROWSER) {
      window.location = apiService.loginUrl;
    }
  } else if (data.code === 200) {
    openNotificationWithIcon('success', 'Success', data.response.message);
  } else {
    openNotificationWithIcon('error', 'Error', data.error);
    throw data.error.toString();
  }
  return data;
};

export const truncate = (text, length = 30) =>
  text && text.length > length ? (
    <Tooltip title={text}>{`${text.substr(0, length)}..`}</Tooltip>
  ) : (
    text
  );

export const handleSelect = (data, key = 'id', value = 'title', t = false) => {
  try {
    if (!t) t = text => text;
    if (data && data.length > 0) {
      return data.map(item => (
        (item && typeof(item) !== undefined) ?
          <Option key={item[key]} value={item[key]} title={t(item[value])}>
            {truncate(t(item[value]), 50)}
          </Option> : <Option key='NA'/>
      ));
    }
  } catch (e) {
    console.error(e);
    return <Option/>
  }
};
