/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from "react";
import withStyles from "isomorphic-style-loader/lib/withStyles";
import s from "./ChangeUserForm.css";
import { Form, Input, Icon, Modal } from "antd";

const FormItem = Form.Item;

class ChangeUserForm extends React.Component {
  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.state = { formId: false };
  }

  componentWillReceiveProps(nextProps) {
    const data = nextProps.data;
    if (data.id && nextProps.visible && !this.state.formId) {
      nextProps.form.setFieldsValue(data);
      this.setState({ formId:  data.id });

    } else if (!nextProps.visible) {
      this.setState({ formId: false });
    }
  }

  onSubmit(e = false) {
    if (e) e.preventDefault();
    this.setState({ loading: true });
    try {
      this.props.form.validateFields(async (err, data) => {
        if (!err) {
          this.props.onUserSubmit(data, this.props.editMode);
        }
      });
    } catch (e) {
      console.log(e);
      this.setState({ loading: false });
    }
  }

  handleConfirmPassword = (rule, value, callback) => {
    const { getFieldValue } = this.props.form;
    if (!value) {
      callback("Confirm Password must equal password");
      return false;
    }
    if (value !== getFieldValue("password")) {
      callback("Confirm Password must equal password");
      return false;
    }
    callback();
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { props } = this;
    return (
      <Modal
        visible={props.visible}
        title={props.editMode ? `Modify ${props.data.username}` : "Add New Admin User"}
        okText={props.editMode ? `Modify` : `Save`}
        onOk={this.onSubmit}
        onCancel={props.onClose}
        destroyOnClose
      >
        <Form>
          <FormItem>
            {getFieldDecorator("firstName", {
              rules: [{ whitespace: false, required: true, min: 3, max: 50, message: "Please input your First Name!" }]
            })(
              <Input
                prefix={<Icon type="user" style={{ color: "rgba(0,0,0,.25)" }}/>}
                placeholder="First Name"
              />
            )}
          </FormItem>
          <FormItem>
            {getFieldDecorator("lastName", {
              rules: [{ whitespace: false, required: true, min: 3, max: 50, message: "Please input your Last Name!" }]
            })(
              <Input
                prefix={<Icon type="user" style={{ color: "rgba(0,0,0,.25)" }}/>}
                placeholder="Last Name"
              />
            )}
          </FormItem>
          <FormItem>
            {getFieldDecorator("username", {
              rules: [{ whitespace: false, required: true, min: 3, max: 50, message: "Please input your Username!" }]
            })(
              <Input
                prefix={<Icon type="user" style={{ color: "rgba(0,0,0,.25)" }}/>}
                placeholder="Username"
                disabled={props.selfMode}
              />
            )}
          </FormItem>
          <FormItem>
            {getFieldDecorator("email", {
              rules: [{ whitespace: false, required: true, min: 6, max: 50, message: "Please input your Username!" }]
            })(
              <Input
                prefix={<Icon type="mail" style={{ color: "rgba(0,0,0,.25)" }}/>}
                placeholder="email"
                disabled={props.selfMode}
              />
            )}
          </FormItem>
          {!props.editMode && (
            <FormItem>
              {getFieldDecorator("password", {
                rules: [{ whitespace: false, required: true, min: 6, max: 50, message: "Please input your Password!" }]
              })(
                <Input
                  prefix={<Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }}/>}
                  type="password"
                  placeholder="Password"
                />
              )}
            </FormItem>
          )}
          {!props.editMode && (
            <FormItem>
              {getFieldDecorator("passwordConfirm", {
                rules: [
                  { whitespace: false, required: true, min: 6, max: 50, message: "Please input your Password Confirm!" },
                  { validator: this.handleConfirmPassword, message: "Confirm Password must equal password" }
                ]
              })(
                <Input
                  prefix={<Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }}/>}
                  type="password"
                  placeholder="Password Confirm"
                />
              )}
            </FormItem>
          )}
        </Form>
      </Modal>
    );
  }
}

export default (Form.create()(withStyles(s)(ChangeUserForm)));
