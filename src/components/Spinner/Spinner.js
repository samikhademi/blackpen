/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Spinner.css';
import cs from 'classnames';

class Spinner extends React.Component {
    constructor(props){
        super(props);
        this.forceUpdateHandler = this.forceUpdateHandler.bind(this);
    };

    componentWillMount(){

        this.props.onLoad(this)
    }

    forceUpdateHandler(){
        this.reset();
    };

    reset() {
        if (this.timer) {
            clearInterval(this.timer);
        }

        this.start = this.setStartPosition();

        this.setState({
            position: this.start,
            timeRemaining: this.props.timer
        });

        this.timer = setInterval(() => {
            this.tick()
        }, 100);
    }

    state = {
        position: 0,
        lastPosition: null
    }
    static iconHeight = 188;
    multiplier = Math.floor(Math.random()*(4-1)+1);

    start = this.setStartPosition();
    speed = Spinner.iconHeight * this.multiplier;

    setStartPosition() {
        return ((Math.floor((Math.random()*8))) * Spinner.iconHeight)*-1;
    }

    moveBackground() {
        this.setState({
            position: this.state.position - this.speed,
            timeRemaining: this.state.timeRemaining - 100
        })
    }

    getSymbolFromPosition() {
        let { position } = this.state;
        const totalSymbols = 8;
        const maxPosition = (Spinner.iconHeight * (totalSymbols-1)*-1);
        let moved = (this.props.timer/100) * this.multiplier
        let startPosition = this.start;
        let currentPosition = startPosition;

        for (let i = 0; i < moved; i++) {
            currentPosition -= Spinner.iconHeight;

            if (currentPosition < maxPosition) {
                currentPosition = 0;
            }
        }
        this.props.onFinish(currentPosition);
    }

    tick() {
        if (this.state.timeRemaining <= 0) {
            clearInterval(this.timer);
            this.getSymbolFromPosition();

        } else {
            this.moveBackground();
        }
    }

    componentDidMount() {
        clearInterval(this.timer);

        this.setState({
            position: this.start,
            timeRemaining: this.props.timer
        });

        this.timer = setInterval(() => {
            this.tick()
        }, 100);
    }

    render() {
        let { position, current } = this.state;

        return (
            <div
                style={{backgroundPosition: '0px ' + position + 'px'}}
                className={cs(this.props.className,s.icons)}
            />
        )
    }
}

export default withStyles(s)(Spinner);
