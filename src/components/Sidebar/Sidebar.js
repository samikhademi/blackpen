/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Sidebar.css';
import Link from '../Link';
import { Icon, Menu, Layout } from "antd";
import { logo, headerLogo } from './../../globalSettings';
import cs from 'classnames';

const { SubMenu } = Menu;
const MenuItem = Menu.Item;
const Sider = Layout.Sider;
const linkMenus = [
  {
    title: 'Blocked Client',
    link: '/blocked-mng',
    icon: 'solution',
  },
];
class Sidebar extends React.Component {
  state = {
    collapsed: false,
  };

  onCollapse = collapsed => {
    this.setState({ collapsed });
  };

  render() {
    return (
      <Sider width='20%' collapsible collapsed={this.state.collapsed} onCollapse={this.onCollapse}>
        <div className={cs(s.topHead,'topHead')}>
          <Link to='/'>
            <img style={{width: '100%'}} src={headerLogo} />
          </Link>
        </div>
        <div className={cs(s.logo,'logo')}>
          <Link to='/'>
            <img src='/images/logoHorColorfulMini.png' />
          </Link>
        </div>
        <Menu theme="dark" mode="inline">
            <MenuItem key="admin-users">
              <Link to='/admin-users'>
                <Icon type="user" />
                <span>Admin Management</span>
              </Link>
            </MenuItem>
            <MenuItem key="countries">
              <Link to='/countries'>
                <Icon type="global" />
                <span>Countries</span>
              </Link>
            </MenuItem>
            <MenuItem key="slots">
              <Link to='/slots'>
                <Icon type="windows" />
                <span>Slots</span>
              </Link>
            </MenuItem>
        </Menu>
      </Sider>
    );
  }
}

export default withStyles(s)(Sidebar);
