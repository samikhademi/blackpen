/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PropTypes from "prop-types";
import withStyles from "isomorphic-style-loader/lib/withStyles";
import normalizeCss from "normalize.css";
import s from "./Layout.css";
import Footer from "../Footer";
import { Layout, PageHeader, Modal, Form, Input, Icon, Spin, Menu, Dropdown, Button } from "antd";
import Sidebar from "../Sidebar";
import { setUser } from "../../actions/user";
import { setToken } from "../../actions/runtime";
import { PostData, deleteCookie, PutData } from "../../utils/rest";
import { apiService } from "../../var";
import history from "./../../history";
import LoadingBar from 'react-redux-loading-bar'
import ChangeUserForm from "../Forms/ChangeUserForm";

const { Content } = Layout;
const FormItem = Form.Item;
const MenuItem = Menu.Item;

class MainLayout extends React.Component {

  static propTypes = {
    children: PropTypes.node.isRequired
  };

  state = { loading: false, visibleChangePasswordModal: false, visibleChangeProfileModal: false, pageLoading: true };

  onEditUser = this.onEditUser.bind(this);

  componentDidMount() {
    this.setState({ pageLoading: false });
  }

  onLogout = () => {
    deleteCookie("token");
    this.props.actions.setToken("");
    history.push("/login");
  };

  showChangePasswordModal = () => this.setState({ visibleChangePasswordModal: !this.state.visibleChangePasswordModal });

  toggleChangeUserModal = () => this.setState({ visibleChangeProfileModal: !this.state.visibleChangeProfileModal });

  async onEditUser(data) {
    this.setState({ loading: true });
    try {
      data.id = this.props.user.user.id;
      await PutData(this.props.token, apiService.services.userController, data, fetch);
      const user = this.props.user;
      user.user.firstName = data.firstName;
      user.user.lastName = data.lastName;
      await this.props.actions.setUser(user);
      this.setState({ visibleChangeProfileModal: false, loading: false });

    } catch (e) {
      console.log(e);
      this.setState({ loading: false });
    }
  }

  onSubmitChangePassword = (e = false) => {
    if (e) e.preventDefault();
    this.setState({ loading: true });
    try {
      this.props.form.validateFields(async (err, data) => {
        if (!err) {
          data.mustPasswordChange = false;
          const response = await PostData(this.props.token, apiService.services.changePassword, data, fetch);
          if (response.code === 200) {
            const user = this.props.user;
            user.user.mustPasswordChange = false;
            await this.props.actions.setUser(user);
            this.setState({ loading: false, visibleChangePasswordModal: false });
          }
        }
      });
    } catch (e) {
      console.log(e);
      this.setState({ loading: false });
    }
  };

  handleConfirmPassword = (rule, value, callback) => {
    const { getFieldValue } = this.props.form;
    if (!value) {
      callback("Confirm Password must equal password");
      return false;
    }
    if (value !== getFieldValue("password")) {
      callback("Confirm Password must equal password");
      return false;
    }
    callback();
  };

  render() {
    const routes = [
      {
        key: '/',
        path: "/",
        breadcrumbName: "Dashboard Home"
      }
    ];
    const { user } = this.props.user;
    const { getFieldDecorator } = this.props.form;
    if (this.props.title) {
      routes.push({ key: `/${this.props.title}`, path: "", breadcrumbName: this.props.title });
    }
    const menu = (
      <Menu>
        <MenuItem key='Change Profile' onClick={this.toggleChangeUserModal}>Change Profile</MenuItem>
        <MenuItem key='Change Password' onClick={this.showChangePasswordModal}>Change Password</MenuItem>
        <MenuItem key='Logout' onClick={this.onLogout}>Logout</MenuItem>
      </Menu>
    );

    let userProfile = <div/>;
    if(user) {
      userProfile = <Dropdown overlay={menu} placement="bottomLeft"><Button
          icon='user'>{`${user.username}`}</Button></Dropdown>;
    }

    return (
      <div>
        {this.state.pageLoading && (
          <Spin style={{ height: 500, textAlign: 'center', paddingTop: 250, margin: '0 auto', width: '100%' }} size='large' spinning={true} tip='Please wait for loading'/>
        )}
        {!this.state.pageLoading && (
          <Layout style={{ minHeight: "100vh" }}>
            <LoadingBar style={{ zIndex: 1000 }} />
            <Sidebar/>
            <Layout>
              <Content>
                {this.props.title && (
                  <PageHeader extra={[userProfile]} title={this.props.title} breadcrumb={{ routes }}/>
                )}
                <div className={s.contents}>{this.props.children}</div>
              </Content>
              <Footer/>
              <Modal
                  closable={false}
                  loading={this.state.loading}
                  okText='Change Password'
                  title='Change Self Password'
                  visible={user && user.mustPasswordChange}
                  onOk={this.onSubmitChangePassword}
                  cancelButtonProps={{ style: { display: "none" } }}
              >
                <p className={s.textCenter}>
                  <Icon type="safety" style={{ fontSize: "65px", display: "block", marginBottom: "15px" }}/>
                  You must change your password before working in back office
                </p>
                <Form onSubmit={this.onSubmit} className={s.loginForm}>
                  <FormItem>
                    {getFieldDecorator("password", {
                      rules: [{ required: true, min: 6, max: 50, message: "Please input your Password!" }]
                    })(
                        <Input
                            prefix={<Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }}/>}
                            type="password"
                            placeholder="Password"
                        />
                    )}
                  </FormItem>
                  <FormItem>
                    {getFieldDecorator("passwordConfirm", {
                      rules: [
                        { required: true, min: 6, max: 50, message: "Please input your Password Confirm!" },
                        { validator: this.handleConfirmPassword, message: "Confirm Password must equal password" }
                      ]
                    })(
                        <Input
                            prefix={<Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }}/>}
                            type="password"
                            placeholder="Password Confirm"
                        />
                    )}
                  </FormItem>
                </Form>
              </Modal>
              <Modal
                  destroyOnClose
                  onCancel={this.showChangePasswordModal}
                  loading={this.state.loading}
                  okText='Change Password'
                  title='Change Self Password'
                  onOk={this.onSubmitChangePassword}
                  visible={this.state.visibleChangePasswordModal}
              >
                <p className={s.textCenter}>
                  <Icon type="safety" style={{ fontSize: "65px", display: "block", marginBottom: "15px" }}/>
                </p>
                <Form onSubmit={this.onSubmit} className={s.loginForm}>
                  <FormItem>
                    {getFieldDecorator("password", {
                      rules: [{ required: true, min: 6, max: 50, message: "Please input your Password!" }]
                    })(
                        <Input
                            prefix={<Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }}/>}
                            type="password"
                            placeholder="Password"
                        />
                    )}
                  </FormItem>
                  <FormItem>
                    {getFieldDecorator("passwordConfirm", {
                      rules: [
                        { required: true, min: 6, max: 50, message: "Please input your password confirm!" },
                        { validator: this.handleConfirmPassword, message: "Confirm password must equal password" }
                      ]
                    })(
                        <Input
                            prefix={<Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }}/>}
                            type="password"
                            placeholder="Password Confirm"
                        />
                    )}
                  </FormItem>
                </Form>
              </Modal>
              <ChangeUserForm
                  visible={this.state.visibleChangeProfileModal}
                  editMode={true}
                  data={this.props.user.user}
                  onUserSubmit={this.onEditUser}
                  onClose={this.toggleChangeUserModal}
                  selfMode={true}
                  loading={this.state.loading}
              />

            </Layout>
          </Layout>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.user,
  token: state.runtime.token
});

const mapDispatchProps = dispatch => ({
  actions: bindActionCreators(
    { setUser, setToken },
    dispatch
  ),
  dispatch
});
export default connect(mapStateToProps, mapDispatchProps)(Form.create()(withStyles(normalizeCss, s)(MainLayout)));