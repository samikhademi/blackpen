/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Footer.css';
import { app } from './../../globalSettings'
import Link from '../Link';
import { Layout } from "antd";
const AntFooter = Layout.Footer;
class Footer extends React.Component {
  render() {
    return (
      <AntFooter className={s.textCenter}>
        Copyright © 2019 by Black Pen <br/>
        <span style={{ fontSize: 10 }}>v {app.version}</span>
      </AntFooter>
    );
  }
}

export default withStyles(s)(Footer);
