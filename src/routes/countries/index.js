/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import Countries from './Countries';
import Layout from '../../components/MainLayout';
const title = 'Countries';
function action() {
  return {
    title,
    chunks: ['countries'],
    component: (
      <Layout title={title}>
        <Countries/>
      </Layout>
    ),
  };
}

export default action;
