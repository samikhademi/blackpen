/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import s from './Countries.css';
import {Select, Button, Modal, Spin, Row, Col} from 'antd';
import {FetchData, PostData} from './../../utils/rest';
import {apiService} from './../../var';

const Option = Select.Option;

class Countries extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            countriesBE: [],
            countriesFE: [],
            countriesBEOffline: [],
            countriesBESelected:  undefined,
            countriesFESelected:  undefined,
            countriesBEOfflineSelected: undefined,
            countriesBEOfflineMultipleSelected: undefined,
            countriesBELoading: false,
            countriesFELoading: false,
            countryGetLoading: false,
        };
        this.lastFetchId = 0;
        this.lastFetchIdFE = 0;
        this.getCountriesBE = this.getCountriesBE.bind(this);
        this.getCountriesFE = this.getCountriesFE.bind(this);
    }

    componentDidMount() {
        this.getCountriesBE();
        this.getCountriesFE();
    }
    getCountriesBE(search = '') {
        this.lastFetchId += 1;
        const fetchId = this.lastFetchId;
        try {
            this.setState({countriesBELoading: true});
            FetchData(this.props.token, apiService.services.getCountries(search), fetch).then(countriesBE => {
                if (fetchId !== this.lastFetchId) return;
                this.setState({countriesBELoading: false, countriesBE, countriesBEOffline: countriesBE});
            }).catch(error=> {
                this.setState({countriesBELoading: false});
                throw error;
            })
        } catch (e) {
            console.log(e);
            this.setState({countriesBELoading: false});
        }
    }

    getCountriesFE(search = false) {
        this.lastFetchIdFE += 1;
        const fetchId = this.lastFetchIdFE;
        try {
            this.setState({countriesFELoading: true});
            const url = !search ? apiService.services.getCountriesFE : apiService.services.getCountriesFESearch(search)
            FetchData(false,url, fetch).then(countriesFE => {
                if (fetchId !== this.lastFetchIdFE) return;
                this.setState({countriesFELoading: false, countriesFE});
            }).catch(error=> {
                this.setState({countriesFELoading: false});
                throw error;
            })
        } catch (e) {
            console.log(e);
            this.setState({countriesFELoading: false});
        }
    }

    getCountry(selected){
        try {
            this.setState({countryGetLoading: true});
            FetchData(this.props.token, apiService.services.getCountry(selected), fetch).then(country => {
                this.setState({countryGetLoading: false});
                Modal.success({
                    title: country.name,
                    content: this.renderCountry(country),
                })
            }).catch(error=> {
                this.setState({countryGetLoading: false});
                throw error;
            })
        } catch (e) {
            console.log(e);
            this.setState({countryGetLoading: false});
        }

    }

    getCountryByCodes(countriesCode){
        try {
            this.setState({countryGetLoading: true});
            PostData(this.props.token, apiService.services.getCountriesByCode, {countriesCode}, fetch).then(country => {
                this.setState({countryGetLoading: false});
                Modal.success({
                    width: '100%',
                    title: 'list of Countries',
                    content: this.renderCountries(country.response),
                })
            }).catch(error=> {
                this.setState({countryGetLoading: false});
                throw error;
            })
        } catch (e) {
            console.log(e);
            this.setState({countryGetLoading: false});
        }

    }

    renderCountryCodeFlag(code) {
        try {
            return code.toLowerCase();
        } catch (e) {
            return code;
        }
    }

    renderCountry(country){
        return <Col className={s.countryHolder}>
            <img style={{ maxWidth: '75%' }} src={country.flag}/>
            <h5>capital: {country.capital}</h5>
            <h5>region: {country.region}</h5>
            <h5>calling Codes: {country.callingCodes}</h5>
            <h5>languages: <ul>{country.languages.map(item => <li>{item.name}</li>)}</ul></h5>
            <h5>currencies: <ul>{country.currencies.map(item => <li>{item.name}</li>)}</ul></h5>
            <h5>borders: <ul>{country.borders.map(item => <li>{item}</li>)}</ul></h5>
        </Col>
    }

    renderCountries(countries){
        return <Row type='flex'>{countries.map(country => this.renderCountry(country))}</Row>
    }

    render() {
        const {countriesBE, countriesBEOffline, countriesBELoading, countriesBESelected, countriesBEOfflineSelected, countriesBEOfflineMultipleSelected, countryGetLoading, countriesFELoading, countriesFE, countriesFESelected} = this.state;
        return (
            <div className={s.root}>
                <div className={s.container}>
                    <Row gutter={15}>
                        <Col span={8}>
                            <h3>Get Country From BE With Search Online</h3>
                            <Select
                                showSearch
                                placeholder="Select Country"
                                filterOption={false}
                                className={s.select}
                                allowClear
                                loading={countriesBELoading}
                                onSearch={this.getCountriesBE}
                                notFoundContent={countriesBELoading ? <Spin size="small"/> : null}
                                value={countriesBESelected}
                                onChange={countriesBESelectedData => this.setState({ countriesBESelected: countriesBESelectedData })}
                            >
                                {countriesBE.map(item => <Option key={item.alpha2Code} value={item.alpha2Code}><i
                                    className={`flag ${this.renderCountryCodeFlag(item.alpha2Code)}`}/>{item.name}
                                </Option>)}
                            </Select>
                            <Button onClick={e => this.getCountry(countriesBESelected)} className={s.filterButton} disabled={countriesBELoading || !countriesBESelected}>Get Country</Button>
                        </Col>
                        <Col span={8}>
                            <h3>Get Country From BE With Search Offline</h3>
                            <Select
                                showSearch
                                placeholder="Select Country"
                                optionFilterProp="children"
                                filterOption={(input, option) =>
                                    option.props.fullName.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                }
                                className={s.select}
                                allowClear
                                loading={countriesBELoading}
                                notFoundContent={countriesBELoading ? <Spin size="small"/> : null}
                                value={countriesBEOfflineSelected}
                                onChange={countriesBEOfflineSelectedData => this.setState({ countriesBEOfflineSelected: countriesBEOfflineSelectedData })}
                            >
                                {countriesBEOffline.map(item => <Option key={item.alpha2Code} fullName={item.name} value={item.alpha2Code}><i
                                    className={`flag ${this.renderCountryCodeFlag(item.alpha2Code)}`}/>{item.name}
                                </Option>)}
                            </Select>
                            <Button onClick={e => this.getCountry(countriesBEOfflineSelected)} className={s.filterButton} disabled={countriesBELoading || !countriesBEOfflineSelected}>Get Country</Button>
                        </Col>
                        <Col span={8}>
                            <h3>Get Country From BE With Multiple</h3>
                            <Select
                                showSearch
                                placeholder="Select Country"
                                optionFilterProp="children"
                                filterOption={(input, option) =>
                                    option.props.fullName.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                }
                                mode='multiple'
                                className={s.select}
                                allowClear
                                loading={countriesBELoading}
                                notFoundContent={countriesBELoading ? <Spin size="small"/> : null}
                                value={countriesBEOfflineMultipleSelected}
                                onChange={countriesBEOfflineMultipleSelected => this.setState({ countriesBEOfflineMultipleSelected })}

                            >
                                {countriesBEOffline.map(item => <Option key={item.alpha2Code} fullName={item.name} value={item.alpha2Code}><i
                                    className={`flag ${this.renderCountryCodeFlag(item.alpha2Code)}`}/>{item.name}
                                </Option>)}
                            </Select>
                            <Button onClick={e => this.getCountryByCodes(countriesBEOfflineMultipleSelected)} className={s.filterButton} disabled={countriesBELoading || !countriesBEOfflineMultipleSelected}>Get All</Button>
                        </Col>
                        <Col span={8}>
                            <h3>Get Country From FE With Search Online</h3>
                            <Select
                                showSearch
                                placeholder="Select Country Front End"
                                filterOption={false}
                                className={s.select}
                                allowClear
                                loading={countriesFELoading}
                                onSearch={this.getCountriesFE}
                                notFoundContent={countriesBELoading ? <Spin size="small"/> : null}
                                value={countriesFESelected}
                                onChange={countriesFESelectedData => this.setState({ countriesFESelected: countriesFESelectedData })}
                            >
                                {countriesFE.map(item => <Option key={item.alpha2Code} value={item.alpha2Code}><i
                                    className={`flag ${this.renderCountryCodeFlag(item.alpha2Code)}`}/>{item.name}
                                </Option>)}
                            </Select>
                        </Col>

                    </Row>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    token: state.runtime.token,
    user: state.user.user,
});

const mapDispatchProps = dispatch => ({
    actions: bindActionCreators(
        {},
        dispatch,
    ),
    dispatch,
});

export default connect(mapStateToProps, mapDispatchProps)(withStyles(s)(Countries));
