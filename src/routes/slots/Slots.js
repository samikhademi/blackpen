/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import s from './Slots.css';
import {Icon} from 'antd'
import Spinner from './../../components/Spinner';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
function RepeatButton(props) {
    return (
        <button className={s.repeatButton} onClick={props.onClick}>
            <Icon type='reload'/>
        </button>
    );
}

class Slots extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            score: 20,
        }
        this.finishHandler = this.finishHandler.bind(this)
        this.handleClick = this.handleClick.bind(this);
        this.spinners = [
            ['cherry', 'lemon', 'apple', 'lemon', 'banana', 'banana', 'lemon', 'lemon'],
            ['lemon', 'apple', 'lemon', 'lemon', 'cherry', 'apple', 'banana', 'lemon'],
            ['lemon', 'apple', 'lemon', 'apple', 'cherry', 'lemon', 'banana', 'lemon'],
        ]
    }
    _child1 = false;
    _child2 = false;
    _child3 = false;

    handleClick() {
        this.setState({ winner: null });
        this.emptyArray();

        this._child1.forceUpdateHandler();
        this._child2.forceUpdateHandler();
        this._child3.forceUpdateHandler();
    }

    static matches = [];

    fixMinus(no){
        if(no < 0) no = no * -1;
        if(no > 1504) no = (no % 1504);
        return no;
    }

    renderIndex(index){
        if(index > 7) index = index % 8;
        return index;
    }

    calcResultScore(result){
        if (result.cherry === 3) return 50;
        if (result.cherry === 2) return 40;
        if (result.apple === 3) return 20;
        if (result.apple === 2) return 10;
        if (result.banana === 3) return 15;
        if (result.banana === 2) return 5;
        if (result.lemon === 3) return 3;
        return -1;
    }

    finishHandler(value) {
        Slots.matches.push(value);
        if (Slots.matches.length === 3) {
            const result = [];
            result.push(this.spinners[0][this.renderIndex((this.fixMinus(Slots.matches[0])/188)+1)]);
            result.push(this.spinners[1][this.renderIndex((this.fixMinus(Slots.matches[1])/188)+1)]);
            result.push(this.spinners[2][this.renderIndex((this.fixMinus(Slots.matches[2])/188)+1)]);

            const reducer = (co, slot) => {
                if (!co[slot]) {
                    co[slot] = 1;
                } else {
                    co[slot] = co[slot] + 1;
                }
                return co;
            }
            const results = this.calcResultScore(result.reduce(reducer, {}));
            this.setState({ score: this.state.score + results });
        }
    }
    emptyArray() {
        Slots.matches = [];
    }
    render() {
        const repeatButton = <RepeatButton onClick={this.handleClick} />

        return (
            <div className={s.container}>
                <h1 className={s.title} style={{ color: 'white'}}>
                    <span>Your Coin: {this.state.score}</span>
                </h1>
                <div className={s.spinnerContainer}>
                    <Spinner className={s.icons1} onFinish={this.finishHandler} onLoad={child => this._child1 = child } timer='1000' />
                    <Spinner className={s.icons2} onFinish={this.finishHandler} onLoad={child => this._child2 = child } timer='1400' />
                    <Spinner className={s.icons3} onFinish={this.finishHandler} onLoad={child => this._child3 = child } timer='2200' />
                    <div className={s.gradientFade}></div>
                </div>
                {repeatButton}
            </div>
        );
    }
}

export default withStyles(s)(Slots);
