/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import Slots from './Slots';
import Layout from '../../components/MainLayout';
const title = 'Slots';
function action() {
  return {
    title,
    chunks: ['slots'],
    component: (
      <Layout title={title}>
        <Slots/>
      </Layout>
    ),
  };
}

export default action;
