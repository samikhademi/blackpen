/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PropTypes from "prop-types";
import { Form, Icon, Input, Button, Spin } from "antd";
import withStyles from "isomorphic-style-loader/lib/withStyles";
import s from "./Login.css";
import { PostData, setCookie } from "./../../utils/rest";
import { apiService } from "./../../var";
import { setToken } from "./../../actions/runtime";
import { setUser } from "./../../actions/user";
import history from "./../../history";

const FormItem = Form.Item;

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = { loading: false };
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(e) {
    e.preventDefault();
    this.props.form.validateFields(async (err, data) => {
      if (!err) await this.doLogin(data);
    });
  }

  async doLogin(data){
    try {
      await this.setState({ loading: true });
      const response = await PostData(null, apiService.services.login, data, fetch, true, false);
      if (response.code === 200) {
        await this.props.actions.setToken(response.response.token);
        await this.props.actions.setUser({user: response.response.user});
        setCookie("token", response.response.token, 24);
        await history.push("/");
      }
      await this.setState({ loading: false });
    } catch (e) {
      await this.setState({ loading: false });
    }
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div className={s.root}>
        <div className={s.container}>
          <Form onSubmit={this.onSubmit} className={s.loginForm}>
            <Spin size='large' spinning={this.state.loading}>
              <div className={s.topTitle}>Admin Login</div>
              <div className={s.holder}>
                <div className={s.textCenter}>
                  <img className={s.logo} src='/images/logo.png'/>
                </div>
                <FormItem>
                  {getFieldDecorator("username", {
                    rules: [{ required: true, min: 3, message: "Please input your username!" }]
                  })(
                    <Input
                      prefix={<Icon type="user" style={{ color: "rgba(0,0,0,.25)" }}/>}
                      placeholder="Username"
                    />
                  )}
                </FormItem>
                <FormItem>
                  {getFieldDecorator("password", {
                    rules: [{ required: true, min: 6, message: "Please input your Password!" }]
                  })(
                    <Input
                      prefix={<Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }}/>}
                      type="password"
                      placeholder="Password"
                    />
                  )}
                </FormItem>
                <Button type="primary" htmlType="submit" className={s.loginFormButton}>
                  Log in
                </Button>
              </div>
            </Spin>
          </Form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  token: state.runtime.token

});

const mapDispatchProps = dispatch => ({
  actions: bindActionCreators(
    { setToken, setUser },
    dispatch
  ),
  dispatch
});
export default connect(mapStateToProps, mapDispatchProps)(Form.create()(withStyles(s)(Login)));