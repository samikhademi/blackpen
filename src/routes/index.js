/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */
/* eslint-disable global-require */

// The top-level (parent) route
const routes = {
  path: '/',

  // Keep in mind, routes are evaluated in order
  children: [
    {
      path: '/',
      load: () => import(/* webpackChunkName: 'home' */ './home'),
    },
    {
      path: '/login',
      load: () => import(/* webpackChunkName: 'login' */ './login'),
    },
    {
      path: '/admin-users',
      load: () => import(/* webpackChunkName: 'admin-users' */ './admin-users'),
    },
    {
      path: '/countries',
      load: () => import(/* webpackChunkName: 'countries' */ './countries'),
    },
    {
      path: '/slots',
      load: () => import(/* webpackChunkName: 'slots' */ './slots'),
    },

    // Wildcard routes, e.g. { path: '*', ... } (must go last)
    {
      path: '*',
      load: () => import(/* webpackChunkName: 'not-found' */ './not-found'),
    },
  ],

  async action({ next, store, query, url }) {
    //if(query.question) store.dispatch(showFreeAsk());
    const route = await next();
    // Provide default values for title, description etc.
    //console.log(url, url);
    route.title = `${route.title || 'Untitled Page'}`;
    route.description = route.description || '';
    return route;
  },
};

// The error page is available by permanent url for development mode
if (__DEV__) {
  routes.children.unshift({
    path: '/error',
    action: require('./error').default,
  });
}

export default routes;
