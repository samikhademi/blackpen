/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import AdminUsers from './AdminUsers';
import Layout from '../../components/MainLayout';
import { showLoading, hideLoading } from 'react-redux-loading-bar'
import { adminUsersLoading, adminUsersLoaded, adminUsersFailure } from './../../actions/adminUsers';
import { apiService } from './../../var';
import { FetchData } from './../../utils/rest';

const title = 'Admin Users';

async function action({ fetch, store }) {
  store.dispatch(hideLoading());
  store.dispatch(showLoading());
  try {
    store.dispatch(adminUsersLoading());
    const data = await FetchData(store.getState().runtime.token, apiService.services.userController, fetch);
    store.dispatch(adminUsersLoaded(data));
  } catch (error) {
    console.log(error)
    store.dispatch(adminUsersFailure(error));
  }

  store.dispatch(hideLoading());

  return {
    title,
    chunks: ['admin-users'],
    component: (
      <Layout title={title}>
        <AdminUsers />
      </Layout>
    ),
  };
}

export default action;
