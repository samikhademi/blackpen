/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './AdminUsers.css';
import { Table, Switch, Button, Divider, Input, Tooltip, Popconfirm } from "antd";
import { adminUsersFailure, adminUsersLoaded, adminUsersLoading } from "../../actions/adminUsers";
import { FetchData, DeleteData, PutData, PostData } from "../../utils/rest";
import moment from "moment";
import { apiService } from "../../var";
import ChangeUserForm from './../../components/Forms/ChangeUserForm';
import ChangeUserPasswordForm from './../../components/Forms/ChangeUserPasswordForm';
const Search = Input.Search;

class AdminUsers extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      loading: false,
      visibleModifyUserModal: false,
      visibleModifyUserModalEditMode: false,
      visibleModifyUserPassword: false,
      visibleModifyUserPasswordUser: false,
      visibleModifyUserModalData: {},
      userPermissionsModalVisible: false,
      userPermissions: [],
    };
    this.reFetchData = this.reFetchData.bind(this);
    this.removeUser = this.removeUser.bind(this);
    this.onAddEditUser = this.onAddEditUser.bind(this);
    this.openAddUserModal = this.openAddUserModal.bind(this);
    this.onCloseUserForm = this.onCloseUserForm.bind(this);
    this.openModifyUserModal = this.openModifyUserModal.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);
    this.getPermissions = this.getPermissions.bind(this);
    this.setPermissions = this.setPermissions.bind(this);
    this.closeModifyPermissionsModal = this.closeModifyPermissionsModal.bind(this);
  }
  columns = [
    {
      title: 'First Name',
      dataIndex: 'firstName',
      key: 'firstName',
    },
    {
      title: 'Last Name',
      dataIndex: 'lastName',
      key: 'lastName',
    },
    {
      title: 'Username',
      dataIndex: 'username',
      key: 'username',
    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: 'Active',
      dataIndex: 'active',
      key: 'active',
      render: (checked, record) => <Switch onChange={() => this.toggleUser(record.id)} checked={checked} />
    },
    {
      title: 'Actions',
      key: 'actions',
      render: record => <div>
          <Tooltip title='Modify'>
            <Button type='primary' icon='edit' onClick={() => this.openModifyUserModal(record)} />
            <Divider type='vertical' />
          </Tooltip>
          <Tooltip title='Change Password'>
            <Button icon='lock' onClick={() => this.openModifyUserPasswordModal(record)} />
            <Divider type='vertical' />
          </Tooltip>
          <Tooltip title='Remove'>
            <Popconfirm title='Are sure you want remove user?' onConfirm={() => this.removeUser(record.id)}>
              <Button type='danger' icon='delete' />
            </Popconfirm>
            <Divider type='vertical' />
          </Tooltip>
          <Tooltip title='Force To Change Password'>
            <Popconfirm title='Are sure you want force user to change Password?' onConfirm={() => this.forceUserChangePassword(record.id)}>
              <Button disabled={record.mustPasswordChange} type='default' icon='block' />
            </Popconfirm>
          </Tooltip>
      </div>
    },
  ];
  search = '';

  toggleLoading = () => this.setState({ loading: !this.state.loading });

  async onAddEditUser(data,editMode){
    this.toggleLoading();
    try {
      if (editMode){
        data.id = this.state.visibleModifyUserModalData.id
        await PutData(this.props.token, apiService.services.userController, data, fetch);
      } else {
        await PostData(this.props.token, apiService.services.userController, data, fetch);
      }
      this.setState({ visibleModifyUserModal: false, visibleModifyUserModalEditMode: false, loading: false })
      this.reFetchData();
    } catch (e) {
      console.log(e);
      this.toggleLoading();
    }
  }

  async onChangePassword(data){
    this.toggleLoading();
    try {
      data.username = this.state.visibleModifyUserPasswordUser;
      await PostData(this.props.token, `${apiService.services.userController}/changePassword`, data, fetch);
      this.setState({ visibleModifyUserPassword: false, visibleModifyUserPasswordUser: false, loading: false })
    } catch (e) {
      console.log(e);
      this.toggleLoading();
    }
  }

  async reFetchData(pagination = {current: 1, pageSize: 10}, search = false){
    try {
      this.toggleLoading();
      if (!search) search = this.search;
      this.props.actions.adminUsersLoading();
      const data = await FetchData(this.props.token,`${apiService.services.userController}?pageSize=${pagination.pageSize}&current=${pagination.current}&search=${search}`, fetch);
      this.props.actions.adminUsersLoaded(data);
      this.search = search;
      this.toggleLoading();
    } catch (error) {
      console.log(error)
      this.toggleLoading();
      this.props.actions.adminUsersFailure(error);
    }
  }

  async removeUser(id){
    try{
      this.toggleLoading();
      await DeleteData(this.props.token, `${apiService.services.userController}/${id}`, fetch);
      let users = this.props.data;
      users.list = users.list.filter(item => item.id !== id);
      this.props.actions.adminUsersLoaded(users);
      this.toggleLoading();
    }catch (e) {
      console.log(e);
      this.toggleLoading();
    }
  }

  async toggleUser(id){
    try{
      this.toggleLoading();
      await PutData(this.props.token, `${apiService.services.userController}/toggleActive/${id}`,{}, fetch);
      let users = this.props.data;
      users.list = users.list.map(item => {
        if (item.id === id) item.active = !item.active;
        return item;
      });
      this.props.actions.adminUsersLoaded(users);
      this.toggleLoading();
    }catch (e) {
      console.log(e);
      this.toggleLoading();
    }
  }

  async getPermissions(id){
    try{
      this.toggleLoading();
      const userPermissions = await FetchData(this.props.token, `${apiService.services.userController}/getPermissions/${id}`, fetch);
      this.setState({ userPermissions, loading: false, userPermissionsModalVisible: true  });
    }catch (e) {
      this.toggleLoading();
      console.log(e);
    }
  }

  async setPermissions(id, permissions){
    try{
      this.toggleLoading();
      await PostData(this.props.token, `${apiService.services.userController}/setPermission/${id}`, { permissions }, fetch);
      this.setState({ loading: false, userPermissionsModalVisible: false  });
    }catch (e) {
      this.toggleLoading();
      console.log(e);
    }
  }

  async forceUserChangePassword(id){
    try{
      this.toggleLoading();
      await PutData(this.props.token, `${apiService.services.userController}/forceChangePassword/${id}`,{}, fetch);
      let users = this.props.data;
      users.list = users.list.map(item => {
        if (item.id === id) item.mustPasswordChange = true;
        return item;
      });
      this.props.actions.adminUsersLoaded(users);
      this.toggleLoading();
    }catch (e) {
      console.log(e);
      this.toggleLoading();
    }
  }

  openAddUserModal(){
    this.setState({
      visibleModifyUserModal: true,
      visibleModifyUserModalEditMode: false,
      visibleModifyUserModalData: {},
    })
  }

  onCloseUserForm(){
    this.setState({
      visibleModifyUserModal: false,
      visibleModifyUserPassword: false,
      visibleModifyUserModalEditMode: false,
      visibleModifyUserModalData: {},
    })
  }

  openModifyUserModal(visibleModifyUserModalData){
    this.setState({
      visibleModifyUserModal: true,
      visibleModifyUserModalEditMode: true,
      visibleModifyUserModalData,
    })
  }

  openModifyUserPasswordModal(record){
    this.setState({
      visibleModifyUserPassword: true,
      visibleModifyUserPasswordUser: record.username,
    })
  }

  closeModifyPermissionsModal(){
    this.setState({
      userPermissionsModalVisible: false,
      userPermissions: [],
    })
  }

  render() {
    const { loading, error, data } = this.props;
    return (
      <div className={s.root}>
        <div className={s.container}>
          <div className={s.textRight}>
            <Search
              allowClear
              onChange={  e =>{
                this.search  = e.target.value;
                this.reFetchData({current: 1, pageSize: 10}, e.target.value)
              }}
              placeholder='Search for users...'
              style={{ width: 300, margin: '10px 0' }}
            />
            <Divider type='vertical' />
            <Button onClick={this.openAddUserModal} icon='user-add' type='primary'>Create Admin</Button>
          </div>
          <Table
            loading={this.state.loading || loading }
            onChange={(pagination) => this.reFetchData(pagination)}
            pagination={{ current: data.current, total: data.total,size: data.pageSize  } }
            dataSource={data.list}
            columns={this.columns}
          />
          <ChangeUserForm
            visible={this.state.visibleModifyUserModal}
            editMode={this.state.visibleModifyUserModalEditMode}
            data={this.state.visibleModifyUserModalData}
            onUserSubmit={this.onAddEditUser}
            onClose={this.onCloseUserForm}
            selfMode={this.props.user.user.username === this.state.visibleModifyUserModalData.username}
          />
          <ChangeUserPasswordForm
            visible={this.state.visibleModifyUserPassword}
            onUserSubmit={this.onChangePassword}
            onClose={this.onCloseUserForm}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  token: state.runtime.token,
  user: state.user.user,
  data: state.adminUsers.data,
  error: state.adminUsers.error,
  loading: state.adminUsers.loading,
});

const mapDispatchProps = dispatch => ({
  actions: bindActionCreators(
    { adminUsersFailure, adminUsersLoaded, adminUsersLoading },
    dispatch,
  ),
  dispatch,
});

export default connect(mapStateToProps, mapDispatchProps)(withStyles(s)(AdminUsers));
