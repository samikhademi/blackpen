/* eslint-disable import/prefer-default-export */

// runtime
export const SET_RUNTIME_VARIABLE = 'SET_RUNTIME_VARIABLE';
export const TOKEN = 'TOKEN';
// user
export const USER_DATA = 'USER_DATA';
export const USER_BALANCE = 'USER_BALANCE';
// admin Users
export const ADMIN_USERS_LOADING = 'ADMIN_USERS_LOADING';
export const ADMIN_USERS_LOADED = 'ADMIN_USERS_LOADED';
export const ADMIN_USERS_FAILURE = 'ADMIN_USERS_FAILURE';
// log Users
export const LOGS_LOADING = 'LOGS_LOADING';
export const LOGS_LOADED =  'LOGS_LOADED';
export const LOGS_FAILURE = 'LOGS_FAILURE';
// Users
export const CLIENTS_LOADING = 'CLIENTS_LOADING';
export const CLIENTS_LOADED =  'CLIENTS_LOADED';
export const CLIENTS_FAILURE = 'CLIENTS_FAILURE';
// Settlements
export const SETTLEMENTS_LOADING = 'SETTLEMENTS_LOADING';
export const SETTLEMENTS_LOADED =  'SETTLEMENTS_LOADED';
export const SETTLEMENTS_FAILURE = 'SETTLEMENTS_FAILURE';
// Customer Testimonials
export const TESTIMONIALS_LOADING = 'TESTIMONIALS_LOADING';
export const TESTIMONIALS_LOADED =  'TESTIMONIALS_LOADED';
export const TESTIMONIALS_FAILURE = 'TESTIMONIALS_FAILURE';