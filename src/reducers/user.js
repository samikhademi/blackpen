import {USER_DATA,USER_BALANCE} from '../constants'
const initialState = {
  user: {},
  balance: 0
};

export default function user(state = initialState, action) {
  switch (action.type) {
    case USER_DATA:
      return {
        ...state,
        user: action.payload.user,
      };
    case USER_BALANCE:
      return {
        ...state,
        balance: action.payload.balance,
      };

    default:
      return state;
  }
}
