import { combineReducers } from 'redux';
import user from './user';
import runtime from './runtime';
import adminUsers from './adminUsers';
import { loadingBarReducer } from 'react-redux-loading-bar'

export default combineReducers({
  user,
  runtime,
  adminUsers,
  loadingBar: loadingBarReducer,
});
