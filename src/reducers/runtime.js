import {
  SET_RUNTIME_VARIABLE,
  TOKEN,
} from '../constants';

const initState = {
  token: false,
};
export default function runtime(state = initState, action) {
  switch (action.type) {
    case SET_RUNTIME_VARIABLE:
      return {
        ...state,
        [action.payload.name]: action.payload.value,
      };
    case TOKEN:
      return {
        ...state,
        token: action.payload.token,
      };
    default:
      return state;
  }
}
