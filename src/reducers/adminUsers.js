import { ADMIN_USERS_LOADING, ADMIN_USERS_LOADED, ADMIN_USERS_FAILURE } from '../constants';

const initialState = {
  loading: false,
  data: [],
  error: false,
};

export default function adminUsers(state = initialState, action) {
  switch (action.type) {
    case ADMIN_USERS_LOADING:
      return {
        ...state,
        loading: true,
      };
    case ADMIN_USERS_LOADED:
      return {
        ...state,
        data: action.payload.data,
        loading: false,
      };
    case ADMIN_USERS_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
      };
    default:
      return state;
  }
}
