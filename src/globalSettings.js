// variables
export const baseUrl = 'http://localhost:3000/BE/';
export const app = {
  baseUrl: 'http://127.0.0.1:3000',
  baseRealUrl: 'http://localhost',
  postfix: '',
  version: '0.1'
};

// theme settings
export const headerLogo = '/images/logoHorColorful.png';
export const logo = '/images/logo.png';
export const favIcon = '/favicon.ico';

// general settings
export const companyName = 'Black Pen';
export const brandName = 'Black Pen';
export const websiteName = 'Black Pen';
