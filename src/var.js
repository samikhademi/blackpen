import {
  app as globalSettingApp,
  baseUrl as globalSettingBaseUrl,
} from './globalSettings';

export const app = globalSettingApp;
export const apiService = {
  baseUrl: globalSettingBaseUrl,
  loginUrl: '/login',
  services: {
    login: `${globalSettingBaseUrl}login`,
    checkToken: `${globalSettingBaseUrl}login/checkToken`,
    changePassword: `${globalSettingBaseUrl}adminUsers/changePassword`,
    userController: `${globalSettingBaseUrl}adminUsers`,
    getCountries: (search) => `${globalSettingBaseUrl}countries/${search}`,
    getCountriesFE:`https://restcountries.eu/rest/v2/all`,
    getCountriesFESearch: (search) => `https://restcountries.eu/rest/v2/name/${search}`,
    getCountry: (code) => `${globalSettingBaseUrl}countries/getCountry/${code}`,
    getCountriesByCode: `${globalSettingBaseUrl}countries/getCountry`,
  },
  servicesBe: {
    getCountries: 'https://restcountries.eu/rest/v2/all',
    getCountriesBySearch: (name)=> `https://restcountries.eu/rest/v2/name/${name}`,
    getCountryByCode: (code)=> `https://restcountries.eu/rest/v2/alpha/${code}`,
    getCountriesByCode: (codes = [])=> `https://restcountries.eu/rest/v2/alpha?codes=${codes.join(';')}`,
  }
};
const vars = { app, apiService };

export default vars;
