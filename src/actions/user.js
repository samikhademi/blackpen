/* eslint-disable import/prefer-default-export */

import { USER_DATA } from '../constants';

export function setUser(user = null) {
  return {
    type: USER_DATA,
    payload: {
      user,
    },
  };
}