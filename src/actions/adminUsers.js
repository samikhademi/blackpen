/* eslint-disable import/prefer-default-export */

import { ADMIN_USERS_FAILURE, ADMIN_USERS_LOADED, ADMIN_USERS_LOADING } from '../constants';

export function adminUsersLoading() {
  return {
    type: ADMIN_USERS_LOADING,
  };
}

export function adminUsersLoaded(data) {
  return {
    type: ADMIN_USERS_LOADED,
    payload: {
      data,
    },
  };
}

export function adminUsersFailure(error) {
  return {
    type: ADMIN_USERS_FAILURE,
    payload: {
      error,
    },
  };
}