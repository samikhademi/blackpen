/* eslint-disable import/prefer-default-export */

import {
  SET_RUNTIME_VARIABLE,
  TOKEN,
} from '../constants';

export function setRuntimeVariable({ name, value }) {
  return {
    type: SET_RUNTIME_VARIABLE,
    payload: {
      name,
      value,
    },
  };
}

export function setToken(token = false) {
  return {
    type: TOKEN,
    payload: { token },
  };
}
