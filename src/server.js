/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import path from 'path';
import express from 'express';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import nodeFetch from 'node-fetch';
import React from 'react';
import ReactDOM from 'react-dom/server';
import PrettyError from 'pretty-error';
import App from './components/App';
import Html from './components/Html';
import { ErrorPageWithoutStyle } from './routes/error/ErrorPage';
import errorPageStyle from './routes/error/ErrorPage.css';
import createFetch from './createFetch';
import router from './router';
const Sequelize = require('sequelize');
import assets from './assets.json'; // eslint-disable-line import/no-unresolved
import configureStore from './store/configureStore';
import { setRuntimeVariable, setToken } from './actions/runtime';
import { setUser } from './actions/user';
import BEConfig from './BE/config';
import { FetchData } from './utils/rest'
import { apiService } from './var'
const routes = require('./BE/routes');
const config = require('./config');
const {exec} = require('child_process');

//migration functions
async function loadSeeders() {
//start migration
  await new Promise((resolve, reject) => {
    const migrate = exec(
        'npx sequelize db:migrate',
        {env: process.env},
        (err, stdout, stderr) => {
          if (err) {
            reject(err);
          } else {
            resolve();
          }
        }
    );

    // Forward stdout+stderr to this process
    migrate.stdout.pipe(process.stdout);
    migrate.stderr.pipe(process.stderr);
  });

//run seeds
  await new Promise((resolve, reject) => {
    const seed = exec(
        'npx sequelize-cli db:seed:all',
        {env: process.env},
        (err, stdout, stderr) => {
          if (err) {
            reject(err);
          } else {
            resolve();
          }
        }
    );

    // Forward stdout+stderr to this process
    seed.stdout.pipe(process.stdout);
    seed.stderr.pipe(process.stderr);
  });
}
//load db migration
loadSeeders();
//set db authentication
const sequelize = new Sequelize(BEConfig.database.database, BEConfig.database.username, BEConfig.database.password, {
  host: BEConfig.database.host,
  dialect: BEConfig.database.dialect
});
//start db sequelize
sequelize.authenticate();

const app = express();
//
// Tell any CSS tooling (such as Material UI) to use all vendor prefixes if the
// user agent is not known.
// -----------------------------------------------------------------------------
global.navigator = global.navigator || {};
global.navigator.userAgent = global.navigator.userAgent || 'all';

//
// Register Node.js middleware
// -----------------------------------------------------------------------------
app.use(express.static(path.resolve(__dirname, 'public')));
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(bodyParser.json());
process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
//
// register server-side rendering middleware
// -----------------------------------------------------------------------------
//main BE service Provider
//set sequelize to requests
app.use((req,res,next) => {
  req.sequelize = sequelize;
  next();
})
// set context for route
const context = { app, sequelize };

routes(context);

// main react service provider
app.get('*',async (req, res, next) => {
  try {
    const css = new Set();
    // Universal HTTP client
    const fetch = createFetch(nodeFetch, {
      baseUrl: config.api.serverUrl,
      cookie: req.headers.cookie,
    });
    // set Initial State
    const initialState = {
      user: req.user || null,
    };
    // set store config
    const store = configureStore(initialState, {
      fetch,
      // I should not use `history` on server.. but how I do redirection? follow universal-router
    });
    store.dispatch(
      setRuntimeVariable({
        name: 'initialNow',
        value: Date.now(),
      }),
    );
    try {
      // validate token
      if (req.originalUrl !== '/login' && !req.cookies.token) {
        res.redirect('/login');
        return;
      } else if (req.originalUrl === '/login' && req.cookies.token) {
        res.redirect('/');
        return;
      } else if (req.originalUrl !== '/login' && req.cookies.token) {
        // check token with server
        try {
          const user = await FetchData(req.cookies.token, apiService.services.checkToken, false, false);
          store.dispatch(setToken(req.cookies.token));
          store.dispatch(setUser(user.user));
        } catch (error) {
          console.log(`There has been a problem with your fetch operation: ${error.message}`);
          res.cookie('token', '', 0);

          //res.redirect('/login');
        }
      }
    } catch (e) {
      console.log(`There has been a problem with your fetch operation: ${error.message}`);
      store.dispatch(setToken(''));
      res.cookie('token', '', 0);
      res.redirect('/login');
      return;
    }

    // Global (context) variables that can be easily accessed from any React component
    // https://facebook.github.io/react/docs/context.html
    //console.log();
    const context = {
      // Enables critical path CSS rendering
      // https://github.com/kriasoft/isomorphic-style-loader
      insertCss: (...styles) => {
        // eslint-disable-next-line no-underscore-dangle
        styles.forEach(style => css.add(style._getCss()));
      },
      fetch,
      pathname: req.originalUrl,
      // You can access redux through react-redux connect
      store,
      storeSubscription: null,
    };

    const route = await router.resolve({
      ...context,
      path: req.path,
      query: req.query,
    });

    if (route.redirect) {
      res.redirect(route.status || 302, route.redirect);
      return;
    }

    const data = { ...route };
    data.children = ReactDOM.renderToString(
      <App context={context} store={store}>
        {route.component}
      </App>,
    );
    data.styles = [{ id: 'css', cssText: [...css].join('') }];
    data.scripts = [assets.vendor.js];
    if (route.chunks) {
      data.scripts.push(...route.chunks.map(chunk => assets[chunk].js));
    }
    data.scripts.push(assets.client.js);
    data.app = {
      apiUrl: config.api.clientUrl,
      state: context.store.getState(),
    };
    const html = ReactDOM.renderToStaticMarkup(<Html {...data} />);
    res.status(route.status || 200);
    res.send(`<!doctype html>${html}`);
  } catch (err) {
    next(err);
  }
});

//
// Error handling
// -----------------------------------------------------------------------------
const pe = new PrettyError();
pe.skipNodeFiles();
pe.skipPackage('express');

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
  console.error(pe.render(err));
  const html = ReactDOM.renderToStaticMarkup(
    <Html
      title="Internal Server Error"
      description={err.message}
      styles={[{ id: 'css', cssText: errorPageStyle._getCss() }]} // eslint-disable-line no-underscore-dangle
    >
      {ReactDOM.renderToString(<ErrorPageWithoutStyle error={err} />)}
    </Html>,
  );
  res.status(err.status || 500);
  res.send(`<!doctype html>${html}`);
});

//
// Launch the server
// -----------------------------------------------------------------------------
if (!module.hot) {
  (() => {
    app.listen(config.port, () => {
      console.info(`The server is running at http://localhost:${config.port}/`);
    });
  })();
}

//
// Hot Module Replacement
// -----------------------------------------------------------------------------
if (module.hot) {
  app.hot = module.hot;
  module.hot.accept('./router');
}
export default app;
