'use strict';
const express = require('express');
const router = express.Router();
const uuidv4 = require('uuid/v4');
const bcrypt = require('bcryptjs');
const Sequelize = require('sequelize');
const {validationResult} = require('express-validator');
const {validateUserAdd, validateUserModify, formatValidation, validateChangePassword} = require('../middleWare/validator');
const normalize = require('../middleWare/normalizer');

module.exports = context => {
    const {sequelize} = context;
    const usersModel = require('../models/adminUsers')(sequelize);

    router.get('/:username', async (req, res, next) => {
        req.logObject.type = `getAdminUser`;
        try {
            req.permission = 'USER_GET_USERS_INFO';
            let username = req.params.username;
            req.logObject.requestUser = username;
            if (username !== req.user.user.username) {
                if (!req.user.permissions.includes('USER_GET_USERS_INFO')){
                    req.logObject.reason = 'does not have permission to get other user permission';
                    req.logObject = `"${req.user.user.username}" does not have permission to get "${username}'s" user info`;
                    req.errorStatus = 403;
                    throw ('You dont have right permission');
                }
            }

            const response = await usersModel.findOne({where: {username, isRemove: false}});
            if (response) {
                req.result = response;
                req.errorStatus = 200;
            } else {
                req.logObject.reason = `user not found!`;
                req.errorStatus = 404;
                throw ('User Not Found');
            }
        } catch (error) {
            req.error = error;
            req.errorStatus = req.errorStatus ? req.errorStatus : 404;
        }
        next();
    });

    router.get('/', async (req, res, next) => {
        req.logObject.type = `getAdminUsers`;
        try {
            req.permission = 'USER_VIEW_USERS_LIST';
            const Op = Sequelize.Op;
            const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10;
            const current = req.query.current ? parseInt(req.query.current) : 1;
            let search = req.query.search ? req.query.search : '';
            let where = {
                isRemove: false,
                [Op.or]: [
                    {email: {[Op.like]: `%${search}%`}},
                    {username: {[Op.like]: `%${search}%`}},
                    {firstName: {[Op.like]: `%${search}%`}},
                    {lastName: {[Op.like]: `%${search}%`}},
                ]
            };
            const result = await usersModel.findAndCountAll({
                where,
                limit: pageSize,
                offset: current === 1 ? 0 : ((current - 1) * pageSize),
                attributes: {exclude: ['password']}
            });
            if (result) {
                const response = {list: result.rows, total: result.count, current, pageSize}
                req.result = response;
                req.errorStatus = 200;
            } else {
                req.logObject.reason = `can't get users list`;
                throw ('Users Not Found');
            }
        } catch (error) {
            req.error = error;
            req.errorStatus = req.errorStatus ? req.errorStatus : 500;
        }
        next();
    });

    router.post('', validateUserAdd(), async (req, res, next) => {
        try {
            req.logObject.type = `addAdminUser`;
            req.logObject.request = req.body;
            delete req.logObject.request.password;

            req.permission = 'USER_ADD_USERS';
            if (!req.user.permissions.includes('USER_ADD_USERS')){
                req.errorStatus = 403;
                req.logObject.reason = `does not have permission to add users`;
                throw ('You dont have right permission');
            }
            // check service validation
            const validationErrors = validationResult(req);
            if (!validationErrors.isEmpty()) throw formatValidation(validationErrors.array());
            // init salt config
            const salt = bcrypt.genSaltSync(10);
            const {firstName, lastName, password, email, username} = req.body;
            const model = {
                id: uuidv4(),
                username,
                email,
                firstName,
                lastName,
                password: bcrypt.hashSync(password, salt),
            }
            const result = await usersModel.create(model);
            if (result) {
                req.result = 'User Create Successfully';
                req.errorStatus = 200;
            } else {
                req.logObject.reason = "create user Failed";
                throw new Error('Error Creating User');
            }
        } catch (error) {
            console.log(error)
            req.error = error;
            req.errorStatus = req.errorStatus ? req.errorStatus : 500;
        }
        next();
    });

    router.put('', validateUserModify(), async (req, res, next) => {
        req.logObject.type = `editAdminUser`;
        req.logObject.request = req.body;
        try {
            req.permission = 'USER_MODIFY_USERS_INFO';
            // check service validation
            const validationErrors = validationResult(req);
            if (!validationErrors.isEmpty()) throw formatValidation(validationErrors.array());
            // init salt config
            //const salt = bcrypt.genSaltSync(10);
            let {firstName, lastName, username, email, id} = req.body;
            if (id !== req.user.user.id) {
                if (!req.user.permissions.includes('USER_MODIFY_USERS_INFO')){
                    req.logObject.reason = `does not have permission to modify admin user`;
                    req.errorStatus = 403;
                    throw ('You dont have right permission');
                }
            }
            const model = {
                firstName,
                lastName,
                username,
                email,
                //password: bcrypt.hashSync(password, salt),
            }
            const result = await usersModel.update(model, {where: {id, active: true, isRemove: false}});
            if (result) {
                req.result = 'User Modified Successfully';
                req.errorStatus = 200;
            } else {
                req.logObject.reason = `modify user "${username}" failed`;
                throw new Error('Error Modifying User');
            }
        } catch (error) {
            console.log(error)
            req.error = error;
            req.errorStatus = req.errorStatus ? req.errorStatus : 500;
        }
        next();
    });

    router.post('/changePassword', validateChangePassword(), async (req, res, next) => {
        req.logObject.type = `changeAdminUserPassword`;
        req.logObject.user = req.body.username;
        try {
            // init salt config
            const salt = bcrypt.genSaltSync(10);
            const {password} = req.body;
            let username = req.body.username;
            if (!username) {
                username = req.user.user.username
            }
            const model = {password: bcrypt.hashSync(password, salt)}
            if (typeof (req.body.mustPasswordChange) !== 'undefined') model.mustPasswordChange = req.body.mustPasswordChange;
            const result = await usersModel.update(model, {where: {username}});
            if (result) {
                req.result = 'User Change Password Successfully';
                req.errorStatus = 200;
            } else {
                req.logObject.reason = `change password user "${username}" failed`;
                throw new Error('Error Change Password');
            }
        } catch (error) {
            console.log(error)
            req.error = error;
            req.errorStatus = req.errorStatus ? req.errorStatus : 500;
        }
        next();
    });

    router.delete('/:id', async (req, res, next) => {
        req.permission = 'USER_DELETE_USERS';
        req.logObject.type = `removeAdminUserPermission`;
        req.logObject.userId = req.params.id;
        try {
            let result = await usersModel.findOne({where: {id: req.params.id, isRemove: false}});
            if (!result){
                req.logObject.reason = `user not found for  delete`;
                throw ('No User Found');
            }

            if (!req.user.permissions.includes('USER_DELETE_USERS')) {
                req.logObject.reason = `does not have permission to delete users, user "${result.username}"`;
                req.errorStatus = 403;
                throw ('You dont have right permission');
            }
            if (result && result.username === req.user.user.username) {
                req.logObject.reason = `can not delete your self`;
                throw ('You Can Not Delete Your Self!');
            }
            await usersModel.update({ isRemove: true, active: false }, { where: { id: req.params.id, isRemove: false } });
            if (result) {
                req.result = 'User Remove Successfully';
                req.errorStatus = 200;
            } else {
                req.logObject.reason = `delete user "${result.username}" failed`;
                req.errorStatus = 500;
                throw ('User Remove Failed');
            }
        } catch (error) {
            console.log(error)
            req.error = error;
            req.errorStatus = req.errorStatus ? req.errorStatus : 404;
        }
        next();
    });

    router.put('/forceChangePassword/:id', async (req, res, next) => {
        req.permission = 'USER_CHANGE_USERS_PASSWORD';
        req.logObject.type = `forceAdminUserToChangePassword`;
        req.logObject.userId = req.params.id;

        try {
            let result = await usersModel.findOne({where: {id: req.params.id, isRemove: false}});
            if (!req.user.permissions.includes('USER_CHANGE_USERS_PASSWORD')) {
                req.logObject.reason = `does not have permission to force users to change password, user "${result.username}"`;
                req.errorStatus = 403;
                throw ('You dont have this permission');
            }
            if (!result) throw ('No User Found');
            await usersModel.update({mustPasswordChange: true}, {where: {id: req.params.id, isRemove: false}});
            if (result) {
                req.result = 'In Next Login User Must Change Password';
                req.errorStatus = 200;
            } else {
                req.logObject.reason = `force "${result.username}" to change password failed`;
                throw ('Failed To Force Change Password');
            }
        } catch (error) {
            console.log(error)
            req.error = error;
            req.errorStatus = req.errorStatus ? req.errorStatus : 500;
        }
        next();
    });

    router.put('/toggleActive/:id', async (req, res, next) => {
        req.logObject.type = `toggleAdminUserActive`;
        req.logObject.userId = req.params.id;

        try {
            let result = await usersModel.findOne({where: {id: req.params.id, isRemove: false}});
            req.logObject.action = result.active == true ? 'disable' : 'enable'
            if (!result) {
                req.logObject.reason = 'User not found';
                req.errorStatus = 403;
                throw ('No User Found');
            }

            const rs = await usersModel.update({active: !result.active}, {where: {id: req.params.id, isRemove: false}});
            if (rs) {
                req.result = 'User Change Status Success';
                req.errorStatus = 200;
            } else {
                req.logObject.reason = 'User change status failed';
                throw ('User Change Status Failed');
            }
        } catch (error) {
            console.log(error)
            req.error = error;
            req.errorStatus = req.errorStatus ? req.errorStatus : 404;
        }
        next();
    });

    return router;
}