'use strict';
const express = require('express');
const router = express.Router();
const { FetchData, PutData} = require('../middleWare/rest');
const { apiService } = require('../../var');

module.exports = () => {
    router.get('/',  async (req, res, next) => {
        try {
            req.logObject.type = `getCountryList`;
            const result = await FetchData(apiService.servicesBe.getCountries, res);
            req.result = result;

        } catch (error) {
            req.logObject.reason = error;
            req.error = error;
            req.errorStatus = error.status ? error.status : 500;
        }
        next();
    });
    router.get('/:searchName',  async (req, res, next) => {
        try {
            req.logObject.type = `getCountryList`;
            const searchName = req.params.searchName;

            const result = await FetchData(apiService.servicesBe.getCountriesBySearch(searchName), res);
            req.result = result;

        } catch (error) {
            req.logObject.reason = error;
            req.error = error;
            req.errorStatus = error.status ? error.status : 500;
        }
        next();
    });
    router.get('/getCountry/:countryCode',  async (req, res, next) => {
        req.logObject.type = `getCountry`;
        try {
            req.logObject.type = `countryCode`;
            const countryCode = req.params.countryCode;
            const result = await FetchData(apiService.servicesBe.getCountryByCode(countryCode), res);
            req.result = result;
        } catch (error) {
            req.logObject.reason = error;
            req.error = error;
            console.log(error)
            req.errorStatus = error.status ? error.status : 500;
        }
        next();
    });
    router.post('/getCountry',  async (req, res, next) => {
        req.logObject.type = `getCountriesWithCode`;
        try {
            req.logObject.type = `countryCode`;
            const {countriesCode} = req.body;
            if(!countriesCode || countriesCode.length < 1) throw('error get countries code or code is not collection')
            console.log(apiService.servicesBe.getCountriesByCode(countriesCode))
            const result = await FetchData(apiService.servicesBe.getCountriesByCode(countriesCode), res);
            req.result = result;
        } catch (error) {
            req.logObject.reason = error;
            req.error = error;
            console.log(error)
            req.errorStatus = error.status ? error.status : 500;
        }
        next();
    });

    return router;
}