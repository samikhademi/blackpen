'use strict';
const express = require('express');
const jwt = require('jsonwebtoken');
const router = express.Router();
const config = require('../config');
const { validateLogin, validateToken, formatValidation } = require('../middleWare/validator');
const bcrypt = require('bcryptjs');
const { validationResult } = require('express-validator');
const { services } = require('../../var');
const { PostData } = require('../middleWare/rest');
const { checkToken } = require('../middleWare/checkToken');

module.exports = context => {
    const { sequelize } = context;
    const usersModel = require('../models/adminUsers')(sequelize);

    router.post('', validateLogin(), async (req, res, next) => {
        req.logObject = {};
        req.logObject.type = `login`;
        try {
            // check service validation
            const validationErrors = validationResult(req);
            if (!validationErrors.isEmpty()) throw formatValidation(validationErrors.array());
            let {username, password} = req.body;
            const result = await usersModel.findOne({ where: { username, isRemove: false, active: true } });
            if (result) {

                const comparePassword  = await bcrypt.compare(password, result.password);
                if (!comparePassword) {
                    throw 'Username Or Password is Incorrect';
                }
                const token = jwt.sign({
                    id: result.id,
                }, config.jwtKey, {expiresIn: config.jwtExpireTokenTime});

                let user = JSON.parse(JSON.stringify(result));
                delete user.password;
                delete user.active;
                delete user.isRemove;
                req.result = {token, user};
            } else {
                throw 'Username Or Password is Incorrect';
            }
        } catch (error) {
            console.log(error)
            req.logObject.reason = error ? error : 'Login failed';
            req.error = error;
            req.errorStatus = 401;
        }
        next();
    });

    router.get('/checkToken', validateToken(), checkToken, async (req, res, next) => {
        try {
            req.result = { user: req.user };
            
        } catch (error) {
            console.log(error)
            req.error = error;
            req.errorStatus = 401;
        }
        next();
    });

    return router;
}