'use strict';

const bcrypt = require('bcryptjs');
const uuidv4 = require('uuid/v4');
const Sequelize = require('sequelize');
const config = require('../config');

const sequelizeInstance = new Sequelize(config.database.database, config.database.username, config.database.password, {
  host: config.database.host,
  dialect: config.database.dialect
});

module.exports = {
  up: async (queryInterface, sequelize) => {
    const salt = bcrypt.genSaltSync(10);
    const usersModel = require('./../models/adminUsers')(sequelizeInstance);
    const adminUsers = await usersModel.findAll();

    if(!adminUsers.length) {
      const adminData = {
        id: uuidv4(),
        username: 'admin',
        email: 'info@avidadvice.com',
        firstName: 'admin',
        lastName: 'admin',
        mustPasswordChange: true,
        password: bcrypt.hashSync('2wsx@WSX', salt),
      };
      usersModel.create(adminData);
    }
    return true
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
