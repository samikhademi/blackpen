'use strict';
const Sequelize = require('sequelize');

module.exports = (sequelize, a) => {
  const admin_users = sequelize.define('adminUsers', {
    id: {
      type: Sequelize.STRING,
      allowNull: false,
      primaryKey: true,
      unique: true,
    },
    username: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true,
    },
    firstName: {
      type: Sequelize.STRING,
      allowNull: false
    },
    lastName: {
      type: Sequelize.STRING,
      allowNull: false
    },
    email: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true,
    },
    password: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    mustPasswordChange: {
      type: Sequelize.BOOLEAN,
    },
    active: {
      type: Sequelize.BOOLEAN,
    },
    isRemove: {
      type: Sequelize.BOOLEAN,
    },
    createDate: {
      type: Sequelize.DATE,
      allowNull: true,
    },
    modifyDate: {
      type: Sequelize.DATE,
      allowNull: true,

    },
  }, {
    sequelize,
    modelName: 'adminUsers',
    tableName: 'admin_users',
    timestamps: false
  });
  admin_users.associate = function(models) {
    // associations can be defined here
  };
  return admin_users;
};