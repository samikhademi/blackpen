'use strict';
const Sequelize = require('sequelize');

module.exports = (sequelize) => {
  const admin_activity = sequelize.define('adminActivity', {
    admin_user_id: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    type: {
      type: Sequelize.STRING,
      allowNull: false
    },
    details: {
      type: Sequelize.STRING,
    },
    status: {
      type: Sequelize.STRING,
    },
    create_date: {
      type: Sequelize.DATE,
      allowNull: true,
    },
  }, {
    sequelize,
    modelName: 'admin_activity',
    tableName: 'admin_activity',
    timestamps: false,
    freezeTableName: true
  });
  admin_activity.associate = function(models) {
    // associations can be defined here
  };
  return admin_activity;
};