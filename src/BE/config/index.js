const baseServerUrl = 'http://cometricom.com/service/api/';
const publicUrl = 'http://cometricom.com/service/api/';
const dbConfig = require('./config.json');

module.exports = {
    applicationPort: '5000',
    jwtKey: '$@bkPenK->y',
    //jwtExpireTokenTime: 60 * 60 * 24,
    jwtExpireTokenTime: '1d',
    saltKey: '',
    serviceLoginAttr: {
        username: "admin",
        password: "123fdghr45@@",
        clientId: "backoffice.web",
        clientSecret: "76fgRTYHcv%hh",
    },
    database: dbConfig.development,
    baseServerUrl,
    publicUrl,
};