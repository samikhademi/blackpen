const jwt = require('jsonwebtoken');
const normalize = require('./normalizer');
const config = require('../config');

module.exports.checkToken = async (req, res, next) => {
    try {
        if (!req.headers.token) throw ('Token Not Valid');
        res.token = await jwt.verify(req.headers.token, config.jwtKey);
        next();
    } catch (error) {
        res.status(400).json( normalize.error('Token Not Valid', 400) );
    }

}