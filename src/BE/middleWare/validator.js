const { check } = require('express-validator');
const errorMesssage = {
    LENGTH: (min, max) => `field min value must more than ${min} and is required`,
    ISEMAIL: () => `field is not valid email format`,
    MUSTMATCH: () => `must match`
}

module.exports.formatValidation = errors => {
    return errors.map(item => `${item.param} ${item.msg}`);
}
module.exports.validateLogin = () => [
    check('password').trim().isLength({ min: 6, max: 1000 }).withMessage(errorMesssage.LENGTH(6)),
    check('username').trim().isLength({ min: 3, max: 255 }).withMessage(errorMesssage.LENGTH(3)),
];

module.exports.validateToken = () => [
    check('token').trim().isLength({ min: 6, max: 1000 }).withMessage(errorMesssage.LENGTH(6)),
];

module.exports.validateUserAdd = () => [
    check('username').trim().isLength({ min: 3, max: 255 }).withMessage(errorMesssage.LENGTH(3)),
    check('firstName').trim().isLength({ min: 3, max: 255 }).withMessage(errorMesssage.LENGTH(3)),
    check('lastName').trim().isLength({ min: 3, max: 255 }).withMessage(errorMesssage.LENGTH(3)),
    check('email').trim().isLength({ min: 6, max: 255 }).withMessage(errorMesssage.LENGTH(6)).isEmail().withMessage(errorMesssage.ISEMAIL),
    check('password').trim().isLength({ min: 6, max: 1000 }).withMessage(errorMesssage.LENGTH(6)).custom((value,{req, loc, path}) => {
        if (value !== req.body.passwordConfirm) {
            // trow error if passwords do not match
            throw new Error("don't match");
        } else {
            return value;
        }
    }),
    check('passwordConfirm').trim().isLength({ min: 6, max: 1000 }).withMessage(errorMesssage.LENGTH(6)),

];

module.exports.validateUserModify = () => [
    check('username').trim().isLength({ min: 3, max: 255 }).withMessage(errorMesssage.LENGTH(3)),
    check('firstName').trim().isLength({ min: 3, max: 255 }).withMessage(errorMesssage.LENGTH(3)),
    check('lastName').trim().isLength({ min: 3, max: 255 }).withMessage(errorMesssage.LENGTH(3)),
    check('email').trim().isLength({ min: 6, max: 255 }).withMessage(errorMesssage.LENGTH(6)).isEmail().withMessage(errorMesssage.ISEMAIL),

];
module.exports.validateChangePassword = () => [
    check('password').trim().isLength({ min: 6, max: 1000 }).withMessage(errorMesssage.LENGTH(6)).custom((value,{req, loc, path}) => {
        if (value !== req.body.passwordConfirm) {
            // trow error if passwords do not match
            throw new Error("don't match");
        } else {
            return value;
        }
    }),
    check('passwordConfirm').trim().isLength({ min: 6, max: 1000 }).withMessage(errorMesssage.LENGTH(6)),
];
