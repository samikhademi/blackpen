const normalize = require('./normalizer');
const logRequest = require('./logRequest');
module.exports.setResponse = (req, res) => {
    try {
        const statusCode = req.error ? (req.errorStatus ? req.errorStatus : 500) : 200;
        const {body, headers, query, params} = req;
        const header = headers;
        if(body.password) body.password = "****"
        if(body.passwordConfirm) body.passwordConfirm = "****"
        if(header.token) header.token = "<--USER TOKEN-->"
        const details = JSON.stringify(req.logObject);
        const requestDetails = JSON.stringify({body, params, query, header});
        logRequest.log(req.user ? req.user.user.id : '',
            req.permission ? req.permission : '',
            req.method,
            req.originalUrl,
            details,
            req.clientIp,
            statusCode.toString(),
            requestDetails,
            req
        )
    }catch (e) {
        console.log('cant send log', e);
    }
    try {
        if(req.error) throw req.error;
        res.status(200).json(normalize.response(req.result, 200)).end();
    } catch (error) {
        console.log(error);
        res.status(req.errorStatus ? req.errorStatus : 500).json( normalize.error(req.error, req.errorStatus ? req.errorStatus : 500) ).end();
    }
}