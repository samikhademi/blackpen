const jwt = require('jsonwebtoken');
const normalize = require('./normalizer');
const config = require('../config');
const { setResponse } = require('./setResponse');

module.exports.checkToken = async (req, res, next) => {
    try {
        const usersModel = require('../models/adminUsers')(req.sequelize);
        if (!req.headers.token) throw ('Token Not Valid');
        const decodedToken = await jwt.verify(req.headers.token, config.jwtKey);
        const user = await usersModel.findOne({ where: { id : decodedToken.id }, attributes: { exclude: ['password','active','isRemove'] }});
        req.wsToken = decodedToken.wsToken;
        req.user = { user }
        req.logObject = { username: user.username };
        next();
    } catch (error) {
        req.permission = 'USER_TOKEN_NOT_VALID';
        req.error = error;
        req.errorStatus = 400;
        setResponse(req, res, next);
    }
}