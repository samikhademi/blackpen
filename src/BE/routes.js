
const { checkToken } = require('./middleWare/checkToken');
const { setResponse } = require('./middleWare/setResponse');
const adminUsers = require('./controllers/adminUsersController');
const countries = require('./controllers/contryController');
const login = require('./controllers/loginController');

module.exports = context => {
    //application routes
    context.app.use('/BE/countries', checkToken,countries(context), setResponse);
    context.app.use('/BE/adminUsers', checkToken,adminUsers(context), setResponse);
    context.app.use('/BE/login', login(context),setResponse);
}